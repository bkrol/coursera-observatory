package observatory

import scala.collection.mutable

/**
  * 4th milestone: value-added information
  */
object Manipulation {

  val cachedGrids = mutable.Map.empty[Integer, mutable.Map[GridLocation, Temperature]]
  val cachedAverages = mutable.Map.empty[Int, mutable.Map[GridLocation, Temperature]]
  /**
    * @param temperatures Known temperatures
    * @return A function that, given a latitude in [-89, 90] and a longitude in [-180, 179],
    *         returns the predicted temperature at this location
    */
  def makeGrid(temperatures: Iterable[(Location, Temperature)]): GridLocation => Temperature = {
    val tempKey = temperatures.hashCode()
    val grid = cachedGrids.getOrElseUpdate(tempKey, {
      mutable.Map.empty[GridLocation, Temperature]
    })

    gridLocation => grid.getOrElseUpdate(gridLocation,
        {
          Visualization.predictTemperature(temperatures, GridLocation.toLocation(gridLocation))
        })
  }

  /**
    * @param temeperaturess Sequence of known temperatures over the years (each element of the collection
    *                      is a collection of pairs of location and temperature)
    * @return A function that, given a latitude and a longitude, returns the average temperature at this location
    */
  def average(temperaturess: Iterable[Iterable[(Location, Temperature)]]): GridLocation => Temperature = {
    val avgKey = temperaturess.hashCode()
    val averageGrid = cachedAverages.getOrElseUpdate(avgKey, {
      mutable.Map.empty[GridLocation, Temperature]
    })
    gridLocation => averageGrid.getOrElseUpdate(gridLocation, {
          val (sum, size) = temperaturess.map(makeGrid(_)(gridLocation)).foldLeft((0: Temperature,0: Integer))(
            (acc, temp) => (acc._1+temp, acc._2+1)
          )
          sum/size
        })
  }
  /**
    * @param temperatures Known temperatures
    * @param normals      A grid containing the “normal” temperatures
    * @return A grid containing the deviations compared to the normal temperatures
    */
  def deviation(temperatures: Iterable[(Location, Temperature)], normals: GridLocation => Temperature): GridLocation => Temperature = {
   val grid = makeGrid(temperatures)
   gridLocation => grid(gridLocation)-normals(gridLocation)
  }


}

