package observatory

import java.nio.file.Paths
import java.sql.Date
import java.time.LocalDate

import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql._
import org.apache.spark.sql.types._

/**
  * 1st milestone: data extraction
  */
object Extraction {

  Logger.getLogger("org.apache.spark").setLevel(Level.WARN)

  val spark: SparkSession =
    SparkSession
      .builder()
      .appName("observatory")
      .config("spark.master", "local")
      .getOrCreate()

  import spark.implicits._

  val stationsSchema = StructType(
    StructField("STN", StringType) ::
      StructField("WBAN", StringType) ::
      StructField("latitude", FloatType, nullable = false) ::
      StructField("longitude", FloatType, nullable = false) ::
      Nil
  )

  val temperaturesSchema = StructType(
    StructField("STN", StringType) ::
      StructField("WBAN", StringType) ::
      StructField("month", IntegerType) ::
      StructField("day", IntegerType) ::
      StructField("temperature", DoubleType) ::
      Nil
  )


  /**
    * @param year             Year number
    * @param stationsFile     Path of the stations resource file to use (e.g. "/stations.csv")
    * @param temperaturesFile Path of the temperatures resource file to use (e.g. "/1975.csv")
    * @return A sequence containing triplets (date, location, temperature)
    */
  def locateTemperatures(year: Year, stationsFile: String, temperaturesFile: String): Iterable[(LocalDate, Location, Temperature)] = {
    val stations: Dataset[StationsRow] = readStations(fsPath(stationsFile)).filter($"longitude".isNotNull).filter($"latitude".isNotNull)
    val temperatures = readTemperatures(fsPath(temperaturesFile))

    val stationSTN = stations("STN")
    val temperaturesSTN = temperatures("STN")
    val stationWBAN = stations("WBAN")
    val temperaturesWBAN = temperatures("WBAN")
    val joinCondition = stationSTN === temperaturesSTN && stationWBAN === temperaturesWBAN ||
      stationSTN === temperaturesSTN && stationWBAN.isNull && temperaturesWBAN.isNull ||
      stationSTN.isNull && temperaturesSTN.isNull && stationWBAN === temperaturesWBAN

    val resultWithDate = stations.joinWith(temperatures, joinCondition).map(row => (
      Date.valueOf(LocalDate.of(year, row._2.month, row._2.day)),
      Location(row._1.latitude, row._1.longitude),
      fahrenheitToCelsius(row._2.temperature)
    )).collect()
    resultWithDate.map(row => (row._1.toLocalDate, row._2, row._3))
  }

  /**
    * @param records A sequence containing triplets (date, location, temperature)
    * @return A sequence containing, for each location, the average temperature over the year.
    */

  def fahrenheitToCelsius(d: Double): Double = (d - 32) * 5 / 9

  def locationYearlyAverageRecords(records: Iterable[(LocalDate, Location, Temperature)]): Iterable[(Location, Temperature)] = {
    records.groupBy(row => row._2).mapValues(x => x.map(x => x._3).sum / x.size).toList
  }

  def readStations(stationsFile: String): Dataset[StationsRow] = spark.read.schema(stationsSchema).csv(stationsFile).as[StationsRow]

  def readTemperatures(temperatureFile: String): Dataset[TemperaturesRow] = spark.read.schema(temperaturesSchema).csv(temperatureFile).as[TemperaturesRow]

  def fsPath(resource: String): String = Paths.get(getClass.getResource(resource).toURI).toString

}
