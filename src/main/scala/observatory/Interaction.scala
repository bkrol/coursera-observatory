package observatory

import com.sksamuel.scrimage.{Image, Pixel}

/**
  * 3rd milestone: interactive visualization
  */
object Interaction {

  val tileWidth = 256
  val tileHeight = 256

  /**
    * @param tile Tile coordinates
    * @return The latitude and longitude of the top-left corner of the tile, as per http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames
    */
  def tileLocation(tile: Tile): Location = {
    val n = Math.pow(2, tile.zoom)
    Location(
      Math.atan(Math.sinh(Math.PI * (1 - 2 * tile.y / n))).toDegrees,
      tile.x / n * 360 - 180
    )
  }

  /**
    * @param temperatures Known temperatures
    * @param colors       Color scale
    * @param tile         Tile coordinates
    * @return A 256×256 image showing the contents of the given tile
    */
  def tile(temperatures: Iterable[(Location, Temperature)], colors: Iterable[(Temperature, Color)], tile: Tile): Image = {
    val baseX = (Math.pow(2, 8) * tile.x).toInt
    val baseY = (Math.pow(2, 8) * tile.y).toInt
    val alpha = 255
    val pixelTiles = for (
      y <- 0 until tileHeight;
      x <- 0 until tileWidth
    ) yield Tile(x + baseX, y + baseY, tile.zoom + 8)
    val pixels = pixelTiles.par
      .map(tileLocation)
      .map(loc => Visualization.predictTemperature(temperatures, loc))
      .map(temp => Visualization.interpolateColor(colors, temp))
      .map(col => Pixel(col.red, col.green, col.blue, alpha))
      .toArray
    Image(tileWidth, tileHeight, pixels)
  }

  /**
    * Generates all the tiles for zoom levels 0 to 3 (included), for all the given years.
    *
    * @param yearlyData    Sequence of (year, data), where `data` is some data associated with
    *                      `year`. The type of `data` can be anything.
    * @param generateImage Function that generates an image given a year, a zoom level, the x and
    *                      y coordinates of the tile and the data to build the image from
    */
  def generateTiles[Data](
                           yearlyData: Iterable[(Year, Data)],
                           generateImage: (Year, Tile, Data) => Unit
                         ): Unit = {

    def generate0To3xTiles(args: (Year, Data)): Unit = {
      for (
        zoom <- 0 to 3;
        x <- 0 until Math.pow(2, zoom).toInt;
        y <- 0 until Math.pow(2, zoom).toInt
      ) {
        generateImage(
          args._1,
          Tile(x, y, zoom),
          args._2
        )
      }
    }

    yearlyData.foreach(generate0To3xTiles)
  }

}
