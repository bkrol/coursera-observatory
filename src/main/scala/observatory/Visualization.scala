package observatory

import com.sksamuel.scrimage.{Image, Pixel}

/**
  * 2nd milestone: basic visualization
  */
object Visualization {

  /**
    * @param temperatures Known temperatures: pairs containing a location and the temperature at this location
    * @param location     Location where to predict the temperature
    * @return The predicted temperature at `location`
    */
  def predictTemperature(temperatures: Iterable[(Location, Temperature)], location: Location): Temperature = {
    def weighting(x: Location): Double = {
      val p = 2
      1 / math.pow(greatCircleDistance(location, x), p)
    }

    def predictTemperatureHelper: Temperature = {
      lazy val (nominator, denominator) = temperatures.par
        .map(data => (weighting(data._1), data._2))
        .foldLeft((0d, 0d))((agg, data) => (agg._1 + data._1 * data._2, agg._2 + data._1))
      nominator / denominator
    }

    temperatures.par.find(_._1.equals(location)).map(_._2).getOrElse(predictTemperatureHelper)
  }

  /**
    * @param points Pairs containing a value and its associated color
    * @param value  The value to interpolate
    * @return The color that corresponds to `value`, according to the color scale defined by `points`
    */
  def interpolateColor(points: Iterable[(Temperature, Color)], value: Temperature): Color = {
    def interpolatePointAtX(x: Double)(p0: (Double, Double), p1: (Double, Double)): Double = {
      p0._2 + (x - p0._1) * ((p1._2 - p0._2) / (p1._1 - p0._1))
    }

    def pointAtValue(p0: (Double, Double), p1: (Double, Double)): Double = interpolatePointAtX(value)(p0, p1)

    def calculateColorAtValue(p1: (Temperature, Color), p2: (Temperature, Color)): Color = {
      val p1Red = (p1._1, p1._2.red.toDouble)
      val p1Green = (p1._1, p1._2.green.toDouble)
      val p1Blue = (p1._1, p1._2.blue.toDouble)
      val p2Red = (p2._1, p2._2.red.toDouble)
      val p2Green = (p2._1, p2._2.green.toDouble)
      val p2Blue = (p2._1, p2._2.blue.toDouble)

      Color(
        pointAtValue(p1Red, p2Red).round.toInt,
        pointAtValue(p1Green, p2Green).round.toInt,
        pointAtValue(p1Blue, p2Blue).round.toInt
      )
    }

    def calculateInterpolation: Color = {
      lazy val hotterPoints = points.filter(_._1 > value)
      lazy val colderPoints = points.filter(_._1 < value)
      if (hotterPoints.isEmpty) {
        points.maxBy(_._1)._2
      } else if (colderPoints.isEmpty) {
        points.minBy(_._1)._2
      } else {
        lazy val hotterPoint = hotterPoints.minBy(_._1)
        lazy val colderPoint = colderPoints.maxBy(_._1)

        calculateColorAtValue(colderPoint, hotterPoint)
      }
    }

    points.find(_._1.equals(value)).map(_._2).getOrElse(calculateInterpolation)
  }

  /**
    * @param temperatures Known temperatures
    * @param colors       Color scale
    * @return A 360×180 image where each pixel shows the predicted temperature at its location
    */
  def visualize(temperatures: Iterable[(Location, Temperature)], colors: Iterable[(Temperature, Color)]): Image = {
    val alpha = 255
    val width = 360
    val height = 180
    val halfWidth = width/2
    val halfHeight = height/2
    val locations = for (
      lat <- -halfHeight until halfHeight;
      lon <- -halfWidth until halfWidth
    ) yield Location(-lat, lon)
    val pixels = locations.par
      .map(loc => predictTemperature(temperatures, loc))
      .map(temp => interpolateColor(colors, temp))
      .map(col => Pixel(col.red, col.green, col.blue, alpha))
      .toArray
    Image(width, height, pixels)
  }

  def greatCircleDistance(location1: Location, location2: Location): Double = {
    val earthRadius = 6337
    val centralAngle = (location1, location2) match {
      case (a, b) if a.equals(b) => 0
      case (a, b) if Location.isAntipodesOf(a,b) => math.Pi
      case _ => {
        val lat1Rad = math.toRadians(location1.lat)
        val lat2Rad = math.toRadians(location2.lat)
        val lonAbsDiff = math.abs(math.toRadians(location1.lon) - math.toRadians(location2.lon))
        math.acos(math.sin(lat1Rad) * math.sin(lat2Rad) + math.cos(lat1Rad) * math.cos(lat2Rad) * math.cos(lonAbsDiff))
      }
    }
    earthRadius * centralAngle
  }

}

