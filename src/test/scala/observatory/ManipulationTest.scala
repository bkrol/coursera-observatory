package observatory

import org.scalatest.FunSuite
import org.scalatest.prop.Checkers

trait ManipulationTest extends FunSuite with Checkers {

  test("Average of 1,2,3 should be 2") {
    val temp1: Iterable[(Location, Temperature)] = List((Location(0.0,0.0),1.0))
    val temp2: Iterable[(Location, Temperature)] = List((Location(0.0,0.0),2.0))
    val temp3: Iterable[(Location, Temperature)] = List((Location(0.0,0.0),3.0))
    val temps: Iterable[Iterable[(Location, Temperature)]] = List(temp1,temp2,temp3)
    val gridLoc: GridLocation = GridLocation(0,0)

    assert(Manipulation.average(temps)(gridLoc)===2.0)
  }
}
